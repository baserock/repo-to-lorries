{-# LANGUAGE Arrows #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NoMonomorphismRestriction #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Control.Arrow
import Control.Monad (forM_, when)
import Control.Monad.Reader
import Data.Aeson
import Data.Aeson.Encode.Pretty
import Data.ByteString.Lazy hiding (head, find, map, pack)
import Data.List
import Data.Maybe
import Data.Text hiding (head, find, map)
import GHC.Exts
import Prelude hiding (writeFile)
import System.Console.Docopt
import System.Environment (getArgs)
import Text.XML.HXT.Core

patterns :: Docopt
patterns = [docoptFile|USAGE.txt|]

getArgOrExit = getArgOrExitWith patterns

atTag tag = deep (isElem >>> hasName tag)

data Default = Default { revision :: String, remote :: String } deriving Show

data Lorry = Lorry { name :: String, url :: String } deriving Show

data Project = Project { name :: String, path :: String, remote :: String } deriving Show

data Remote = Remote { name :: String, fetch :: String } deriving Show

data Manifest = Manifest { remotes :: [Remote], projects :: [Project], defaul :: Default, origin :: String } deriving Show


getDefault = atTag "default" >>>
  proc x -> do
    revision <- getAttrValue "revision" -< x
    remote <- getAttrValue "remote" -< x
    returnA -< Default { revision = revision, remote = remote }


getRemote = atTag "remote" >>>
  proc x -> do
    name <- getAttrValue "name" -< x
    fetch <- getAttrValue "fetch" -< x
    returnA -< Remote { name = name, fetch = fetch }


getProject = atTag "project" >>>
  proc x -> do
    name <- getAttrValue "name" -< x
    path <- getAttrValue "path" -< x
    remote <- getAttrValue "remote" -< x
    returnA -< Project { name = name, path = path, remote = remote}
 

projectToLorry :: Project -> Reader Manifest Lorry
projectToLorry p = do
  m <- ask
  let rem = case (remote (p :: Project)) of
              "" -> remote (defaul m :: Default)
              x  -> x
  r <- asks $ find (\x -> name (x :: Remote) == rem) . remotes
  let base = case (fetch $ fromJust r) of
              ".." -> origin m ++ "/"
              x    -> x
  return $ Lorry { name = name (p :: Project), url = base ++ name (p :: Project)}


encodeLorry :: Lorry -> ByteString
encodeLorry (Lorry lname lurl) = encodePretty $ Object $
  fromList [(pack lname ,
             Object $ fromList [("type", "git"), ("url", String $ pack lurl)])]


normalizeLorryFilename :: String -> String
normalizeLorryFilename = flip (++) ".lorry" . map (\c -> if c=='/' then '_'; else c)


main :: IO ()
main = do
  args <- parseArgsOrExit patterns =<< getArgs
  file <- args `getArgOrExit` (argument "file")
  baseurl <- args `getArgOrExit` (argument "base-url")
  let a = readDocument [] file
  defaults <- runX $ a >>> getDefault
  remotes <- runX $ a >>> getRemote
  projects <- runX $ a >>> getProject
  let manifest = Manifest remotes projects (head defaults) (baseurl)
  let lorries = flip runReader manifest . projectToLorry <$> projects
  forM_ lorries $ \x -> writeFile (normalizeLorryFilename (name (x :: Lorry))) (encodeLorry x)
