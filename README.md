# Repo To Lorries

repo-to-lorries is a command line application for turning an Android repo
manifest into a set of baserock lorry files so you can mirror all of the
things in it. It is written in Haskell.

## Building

    stack build
    stack install

## Running

    repo-to-lorries <manifest> <base-url>
